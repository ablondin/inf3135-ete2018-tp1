# Travail pratique 1 : Génération d'une scène ASCII

## Format Markdown (supprimer cette section pour la remise)

N'oubliez pas de bien exploiter le format Markdown.

Sauter une ligne pour changer de paragraphe.

Mettre les noms de fichier et bout de code courts entre apostrophes inversés.
Par exemple, si vous parlez du fichier `tp1.c`.

Mettre les longs bouts de code dans des blocs de code (triples apostrophes
inversés). Par exemple, vous pouvez donner un exemple de commande comme suit:
```sh
make
```

Utiliser des listes à puces ou des énumérations le plus possible (plus agréable
à lire). Par exemple, pour décrire le contenu du projet:

- `tp1.c`: Le fichier principal du projet;
- `Makefile`: Un fichier...

Bien aérer le contenu du fichier source (`README.md`). Éviter les longues
lignes dans le fichier Markdown (par exemple, limiter à 80) pour une meilleure
lisibilité.

## Description

Décrivez ici le projet. Commencez d'abord par une description générale, puis
donnez ensuite des détails. Indiquez le contexte dans lequel ce travail est
accompli (cours, sigle, enseignant, université).

## Auteur

Indiquez ici votre prénom, nom et code permanent.

## Fonctionnement

Expliquez d'abord en mots comment faire fonctionner le projet (imaginez que la
personne qui l'utilisera ne connaît rien du projet et souhaite seulement
l'exécuter). En particulier, indiquez les commandes qui doivent être entrées
pour la compilation et l'exécution.

## Tests

Expliquez ici comment lancer la suite de tests automatiques avec la commande
`make check`.

## Dépendances

Indiquez les dépendances du projet, avec lien officiel. Il faudrait au moins
mentionner GCC et [Bats](https://github.com/sstephenson/bats). Utiliser une
liste à puces pour donner la liste des dépendances!

## Références

Indiquez ici les références que vous avez utilisées pour compléter le projet,
avec lien.

## État du projet

Indiquez toutes les tâches qui ont été complétés en insérant un `X` entre les
crochets. Si une tâche n'a pas été complétée, expliquez pourquoi (lors de la
remise, vous pouvez supprimer les phrases précédentes).

* [ ] Le nom du dépôt GitLab est exactement `inf3135-ete2018-tp1` (Pénalité de
  **50%**).
* [ ] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.com/utilisateur/inf3135-ete2018-tp1`
  (Pénalité de **50%**).
* [ ] L'utilisateur `ablondin` a accès au projet en mode *Developer* (Pénalité
  de **50%**).
* [ ] Le dépôt GitLab est un **clone** du [gabarit
  fourni](https://gitlab.com/ablondin/inf3135-ete2018-tp1) (Pénalité de
  **50%**).
* [ ] Le dépôt GitLab est privé (Pénalité de **50%**).
* [ ] Le dépôt contient au moins un fichier `.gitignore`.
* [ ] Le fichier Makefile permet de compiler le projet lorsqu'on entre `make`.
  Il supporte les cibles `html`, `check` et `clean`. Il 
* [ ] Le nombre de tests qui réussissent/échouent avec la `make check` est
  indiqué.
* [ ] Les sections incomplètes du fichier `README.md` ont été complétées.
* [ ] L'en-tête du fichier est documentée.
* [ ] L'en-tête des prototypes est documentée (*docstring*).
* [ ] Le programme ne contient pas de valeurs magiques.
